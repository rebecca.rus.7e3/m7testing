package cat.itb.testing;

import androidx.test.espresso.Espresso;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)

public class MainActivityTest {

    private String USER_TO_BE_TYPED = "username1";
    private String PASS_TO_BE_TYPED = "password1";

    @Test
    public void elements_activity_main_are_displayed() {
        onView(withId(R.id.textView)).check(matches(isDisplayed()));
        onView(withId(R.id.button)).check(matches(isDisplayed()));
    }

    @Test
    public void on_activity_main_have_the_correct_text(){
        onView(withId(R.id.textView)).check(matches(withText(R.string.main_activity_title)));
        onView(withId(R.id.button)).check(matches(withText(R.string.button)));
    }

    @Test
    public void is_next_button_clickable_and_changes_text_to_back_when_clicked(){
        onView(withId(R.id.button)).check(matches(isClickable())).perform(click()).check(matches(withText(R.string.back_button)));

    }

    @Test
    public void is_the_login_form_of_the_main_fragment_working(){
        onView(withId(R.id.username)).perform(replaceText(USER_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.password)).perform(replaceText(PASS_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click()).check(matches(withText("Logged")));
    }

    @Test
    public void check_if_activity_changed_when_the_login_button_is_pressed(){
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.welcome_back_layout)).check(matches(isDisplayed()));
    }

    @Test
    public void check_if_activity_changed_when_the_login_button_is_pressed_and_if_when_other_button_clicked_changes_back(){
        check_if_activity_changed_when_the_login_button_is_pressed();
        onView(withId(R.id.button_back)).perform(click());
        onView(withId(R.id.main_activity_layout)).check(matches(isDisplayed()));
    }

    @Test
    public void check_if_back_button_is_pressed_activity_goes_back(){
        check_if_activity_changed_when_the_login_button_is_pressed();
        Espresso.pressBack();
        onView(withId(R.id.main_activity_layout)).check(matches(isDisplayed()));
    }
    @LargeTest
    @Test
    public void large_test(){
        onView(withId(R.id.username)).perform(replaceText(USER_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.password)).perform(replaceText(PASS_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.welcome_back_layout)).check(matches(isDisplayed()));
        onView(withId(R.id.welcome_back_text)).check(matches(withText("Welcome back "+USER_TO_BE_TYPED)));
        onView(withId(R.id.button_back)).perform(click());
        onView(withId(R.id.username)).check(matches(withText("")));
        onView(withId(R.id.password)).check(matches(withText("")));
    }

    @Rule
    public ActivityScenarioRule<MainActivity> activityRule =
            new ActivityScenarioRule<>(MainActivity.class);

}
